name: Healthier, happier, more effective
description: Solutions to common challenges faced by software developers.
goals:
    personalmanagement: 1
public: users
assignment:
    Assignment:
    - |
        This assignment will be rather different, and probably quite relaxing: it mostly consists of watching non-technical videos providing all kinds of advice that may benefit you as a software developer and as a human.

        The topics covered here will not be tested in an exam. But in case you need any motivation to take them seriously anyway: take another look at the title of this lesson. ☺

    -
        text: |
            For the resources below, take notes in `notes.md` on the pieces of advice that appeal to you, explaining the advice in your own words.
        weight: 2
        4: A couple of well explained highlights (in one's own words) from most of the videos.

    Motivation & discipline:
    -
        link: https://www.wisdomination.com/screw-motivation-what-you-need-is-discipline/
        title: Wisdomination - Screw motivation, what you need is discipline.
        info: "Part 1: Motivation is a counterproductive attitude to productivity. What counts is discipline."
    -
        link: http://www.wisdomination.com/practical-discipline/
        title: Wisdomination - Practical discipline
        info: "Part 2: Tips on how to actually build discipline."

    Imposter syndrome:
    -
        link: https://www.youtube.com/watch?v=ZQUxL4Jm1Lo
        title: Elizabeth Cox - What is imposter syndrome and how can you combat it?
        info: The imposter syndrome is very, very prevalent among software developers. It's important that you recognize it as such.

    -
        link: https://www.youtube.com/watch?v=eqhUHyVpAwE
        title: TheSchoolOfLife - The Impostor Syndrome
        info: Another interesting take on the imposter syndrome.

    Depression & burn-out:
    -
        link: https://www.linkedin.com/pulse/programmers-depression-why-one-talks-8-powerful-ways-jason-humphrey
        title: "Programmers and Depression: Why No One Talks About This and 8 Powerful Ways to Overcome It - article"
        info: Eight sensible things to do that may help you fight off oncoming depression.
    -
        link: https://www.youtube.com/watch?v=ZvwdDQjjxCY&list=PLA1Zmy7vIH6ucg1XH8xGksQulD3BBi25b
        title: "Programmers and Depression: Why No One Talks About This and 8 Powerful Ways to Overcome It - video"
        info: Video version of the above article.
    -
        link: https://www.actstudenthelp.nl/
        title: ACT StudentHelp
        info: Our academy created a surprising useful website with information on all kinds of problems that may crop up during your studies, and links to Saxion facilities that may be of help.

    - |
        *PS*: Why not take a short break now, if you haven't already? 😀

    Health:
    -
        link: https://www.youtube.com/watch?v=js2vfr96iAQ&t=658s
        title: Why you're always tired
        info: A very convincing and actionable video about the three most cliche pieces of lifestyle advice. 
        _topics: sleep, exercise, eat well

        link: https://www.youtube.com/watch?v=yIHyfr-IFkM
        title: Programmers Should DEFINITELY Exercise (If You Don't Want To... DIE?)

    -
        link: https://www.youtube.com/watch?v=AqZ17sZNgg4
        title: "Programmer Posture"

    Productivity:
    -
        link: https://www.youtube.com/watch?v=eG7cPN1uRAA
        title: How to Maximize Your Productivity (As a Software Developer or Learning Programming)
        info: Three potentially very valuable tips for software developers.
        _topics: taking breaks, planning, disabling distractions
    -
        link: https://en.wikipedia.org/wiki/Pomodoro_Technique
        title: Wikipedia - Pomodoro Technique
    -
        link: https://pomofocus.io/
        title: Pomofocus
        info: An online Pomodoro timer. 

    Learning to program:
    -
        link: https://www.youtube.com/watch?v=r-Von8nlupQ
        title: 5 HUGE Tips For New Programmers
        _topics: learn by doing, dunning-kruger, don't guess, (tech stack), health

    Getting and staying in the flow:
    -
        link: https://www.youtube.com/watch?v=RzzYdlqncaw
        title: |
            Programmer Flow State: "In The Zone" Coding
    -
        link: https://heeris.id.au/2013/this-is-why-you-shouldnt-interrupt-a-programmer/
        title: This is why you shouldn't interrupt a programmer
        info: A comic to make the point.
    
    Procrastination:
    -
        link: https://www.youtube.com/watch?v=arj7oStGLkU
        title: Tim Urban - Inside the mind of a master procrastinator
        info: Funny and awfully recognizable explanation of procrastination. Especially the case of non-deadline procrastination, identified near the end of this video, may be an important life realization!

    Things you'll try:
    -
        text: Which (if any) of the items you've noted in `notes.md` will you be actively applying over at least the next two weeks? In what way will you work each of them into your routine, so you won't forget about them?
        4: Three actionable, relevant and well-described interventions, including ways to work them into existing routines.
