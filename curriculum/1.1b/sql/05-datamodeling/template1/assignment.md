# Data modeling

## Case 1: StackOverflow

### Entities

### Logical ERD

We've made a start for you:

```plantuml

' Some settings to make your ERD look better
hide circle
skinparam linetype ortho

entity User {
    *name
}

entity Question {
    *title
    *timestamp
    description
}

User ||--|{ Question
```

### Physical ERD

We've made a start for you:

```plantuml

' Some settings to make your ERD look better
hide circle
skinparam linetype ortho

entity User {
    *id: integer
    --
    *name: text
}

entity Question {
    *id: integer
    --
    *title: text
    *timestamp: timestamp
    *user_id: integer <<FK>>
    description: text
}

User ||--|{ Question
```


## Case 2: Twitter

### Physical ERD

```plantuml

' Some settings to make your ERD look better
hide circle
skinparam linetype ortho

entity TODO {

}

```


## Case 3: *up to you*

```plantuml

' Some settings to make your ERD look better
hide circle
skinparam linetype ortho

entity TODO {
    
}

```