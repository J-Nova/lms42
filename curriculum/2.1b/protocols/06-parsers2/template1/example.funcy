# This is a comment.

# Variables can be assigned like in Python.
welcome = "Welcome"
num = 123
num = num + 1

# Functions are also 'just' values in funcy. So they don't inherently have a name.
# This nameless function adds two numbers. a and b are its parameters.
a b -> a + b

# You can assign a function to a variable, in order to call it later.
add = a b -> a + b

# This is how you call a function. The result would be 7, but it would not be shown.
add 3 4

# We can also call built-in Python functions. The following also demonstrates nested function calls.
print "3 + 4 =" (add 3 4)

# As function are just values, we can assign them to other variables.
plus = add
print "1 + 2 =" (plus 1 2)

# We could also execute a function without giving it a name
print "6 * 7 =" ((a b -> a * b) 6 7)

# Functions can also consist of more than one expression. In that case, the function body should be
# wrapped in parenthesis, and expressions should be separated by a new line character.
# The last expression will be returned.
hello = name -> (
    print "Hello " + name + ""
    print "It is good to see you..."
)
hello (input "Who are you? ")

# A block of expressions can also be used in other places.
print "(3*3) * (3*3) =" (
    a = 3 * 3
    a * a # The last expression is used as the value for the block.
)

# The language supports the usual plus, minus, multiply and divide operators, and applies them
# in the right order
c = add 3+4 8*2+9*3 # c = add (3+4) ((8*2) + (9*3))
print "c =" c

# There is no if/else but you can use a conditional 'ternary operator' instead.
# It works like this: condition ? if_true : if_false
print c==50 ? "All is well" : "Incorrect"

# You can combine multiple ternary operators to do something like elif.
# Optionally, a new line can be put before the ':'. This allows you to write conditionals
# like this:
opt = input "Choose a or b: "
opt=="a" ? print "Apples coming up"
: opt=="b" ? print "Beautiful bananas" 
: print "Well chosen... well chosen."

# Of course, you can also run a block if a condition holds. And the else-part is optional.
1+1 == 2 ? (
    print "The world appears to be sane."
    print "Good!"
)
