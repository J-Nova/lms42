# Roles

| Role | Monthly salary | Description |
| -- | -- | -- |
| CEO | 9500 | Manages and sets the strategy for the company as a whole. Involved with customer relations and hiring. |
| CFO | ? | ? |
| Accountant | ? | ? |
| Office manager | ? | ? |
| Marketeer | ? | ? |
| HR officer | ? | ? |
| Account manager | ? | ? |
| Front-end developer | ? | ? |
| Back-end developer | ? | ? |
| UX designer | ? | ? |
| Insurance analyst | ? | ? |
| Software architect | ? | ? |
| Product owner | ? | ? |



# Project management

