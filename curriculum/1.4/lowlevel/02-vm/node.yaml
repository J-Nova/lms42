name: CPU
description: Discover how CPUs work by implementing a virtual one yourself.
allow_longer: true
goals:
    computerarchitecture: 1
    pointers: 1
resources:
- These videos give a nice overview of what is CPU does, and what machine code and assembler code look like. Don't get hung up on the details, because in this assignment we'll be working for a different CPU (with different instructions) then the ones in the videos.
-
    link: https://www.youtube.com/watch?v=FZGugFqdr60&list=PLH2l6uzC4UEW0s7-KewFLBC1D0l6XRfye&index=8
    title: "The Central Processing Unit (CPU): Crash Course Computer Science #7"
-
    link: https://www.youtube.com/watch?v=QXjU9qTsYCc
    title: How do computers read code?
TODO:
    - bitwise operations are unused in examples

assignment:
    Ask for help: |
        Today you'll be implementing a simulated CPU, for a simple made-up architecture. If that sounds hard, that's because it is indeed not easy. Although your final implementation can be a relatively short program, there are a few things you need to understand well before you can write that implementation. Therefore, for this assignment even more than for other assignments: if you're not making progress for more than 15 minutes, ask for help!

        Before you open up your editor, we recommend that you read the following two sections two or three times, to gain as much understanding from it as you can.

    SAX16: |
        We'll call our made-up computer architecture SAX16, as it's a 16-bit processor, and we want our name to be in line with the AMD64 architecture (also referred to as EM64 or x64) commonly used on PCs.
        
        16-bit means that memory addresses are 16-bit wide, meaning that (without trickery) our system will only be able to handle `2^16 = 65536 = 64 kB` of memory. That <a href="https://quoteinvestigator.com/2011/09/08/640k-enough/" target="_blank">ought to be enough for anyone</a>.

        A program is a file of up to 64 kB that is loaded into the virtual computer's memory starting at address 0. The two bytes starting at address 0 are special: they store the *instruction pointer*, which is the address of the current instruction being executed by the CPU. So all programs must start with a 2-byte integer that refers to the address of the first instruction to be executed. Whenever the *instruction pointer* (so the 16-bit value at address 0) becomes 0, the virtual CPU *halts* (the program ends).

        Our CPU should be able to handle 13 different instructions, identified by an *op-code* (operation code) between 1 and 13 (inclusive). Each instructions gets two *unsigned* integer numbers as *arguments*, further referred to as *arg1* and *arg2*. However, for some instructions one or both arguments are left unused. Each argument can either be:
        - a literal value,
        - the memory address where the value to be used is stored (single indirection), or
        - the memory address where the memory address of the value to be used is stored (double indirection).

        For each instruction the code also specifies an `integer size`, causing it to work on either 8-bit (1 byte), 16-bit (2 byte) or 32-bit (4 byte) integers.

        The instructions are:

        | Op-code | Name | Description |
        | ------ | ---- | ----------- |
        | 0x01 | set | Set arg1 (which cannot be a literal) to the value of arg2. |
        | 0x02 | add | Set arg1 (which cannot be a literal) to arg1 + arg2. |
        | 0x03 | sub | Set arg1 (which cannot be a literal) to arg1 - arg2. As numbers are *unsigned*, an error occurs if arg2 is larger than arg1. |
        | 0x04 | mul | Set arg1 (which cannot be a literal) to arg1 * arg2. |
        | 0x05 | div | Set arg1 (which cannot be a literal) to arg1 / arg2. Division should always round *down* (the `//` operator in Python). |
        | 0x06 | and | Set arg1 (which cannot be a literal) to arg1 & arg2 (a bitwise *and*). |
        | 0x07 | or | Set arg1 (which cannot be a literal) to arg1 \| arg2 (a bitwise *or*). |
        | 0x08 | xor | Set arg1 (which cannot be a literal) to arg1 ^ arg2 (a bitwise *exclusive or*). |
        | 0x09 | if= | Skip the next instruction unless arg1 and arg2 are equal. |
        | 0x0A | if> | Skip the next instruction unless arg1 is larger than arg2. |
        | 0x0B | if< | Skip the next instruction unless arg1 is smaller than arg2. |
        | 0x0C | read | Read a single byte from the console into arg1, allowing the user to type input. |
        | 0x0D | write | Write arg1 as a single byte to the console. |

        With just these instructions, it is possible (but perhaps slow and impractical) to create any program you could implement in Python, given the restriction that it can only interact with the console (so no networking, disk access, etc).

        Instructions, their integer sizes and arguments are layout out in memory as follows:

        | Bytes | Bits | Name | Description |
        | ----- | ---- | ---- | ----------- |
        | 0 | 0-7 | `op-code` | Which instruction to execute. Note that values 0 and 14-255 are unused (*reserved for future use*). If you CPU encounters one of those, it should stop with an error. |
        | 1 | 0-1 | `integer size` | These two bits have four possible values: <ul><li>**0:** 8 bits (1 byte)</li><li>**1:** 16 bits (2 bytes)</li><li>**2:** 32 bits (4 bytes)</li><li>**3:** unused</li></ul> |
        | 1 | 2-3 | `arg1 type` | The way arg1 is provided: <ul><li>**0:** zero &rarr; arg1 has the literal value 0, `arg1 data` is omitted</li><li>**1:** literal &rarr; arg1 is a literal given as (`integer size` bytes of) `arg1 data`</li><li>**2:** single indirection &rarr; arg1 should be loaded from the (`integer size` bytes at) address given by the (2 bytes of) `arg1 data`</li><li>**3:** double indirection &rarr; arg1 should be loaded from the (`integer size` bytes at) address given by the (2 bytes at) address given by the (2 bytes of) `arg1 data`</li></ul> |
        | 1 | 4-5 | `arg2 type` | The way arg2 is provided, the same as for `arg1 type`. |
        | 2-? | 0-7 | `arg1 data` | As specified by `arg1 type`. |
        | ?-? | 0-7 | `arg2 data` | As specified by `arg2 type`. |

        So for instance `arg1 type` is stored in bits 2 and 3 of the byte number 1 (zero-based, so the second byte), where bit 0 would be the least significant bit. Its value can be extracted using a *right shift* and a *bitwise and* in your Python implementation.

        Besides the *instruction pointer* (at addresses 0 and 1) and the instructions themselves, programs may contain `data`. This can contain strings or room for variables, for instance. The programs should of course take care that the CPU doesn't try to *execute* this data; the *instruction pointer* should always point at a valid instruction, and not a data.

        The architecture uses little endian byte ordering.

    Examples:
        Hello world: |
            The following is the contents of the provided program `hello_world1.executable`, where the number on the left of the ':' is the hexadecimal address of the bytes that follow:
            ```
            0000: 02 00
            0002: 0D 04 48
            0005: 0D 04 65
            0008: 0D 04 6C
            000B: 0D 04 6C
            000E: 0D 04 6F
            0011: 0D 04 20
            0014: 0D 04 77
            0017: 0D 04 6F
            001A: 0D 04 72
            001D: 0D 04 6C
            0020: 0D 04 64
            0023: 0D 04 21
            0026: 0D 04 0A
            0039: 01 09 00 00
            ```

            We are displaying the bytes one line per instruction, to make things easier to read. Apart from that, this is exactly what you would find when opening the file in Visual Studio Code using the *Hex Editor* extension.

            As address 0x0000 of memory (and therefore of any program) is the *instruction pointer*, that's the first line. Because the machine is using *little endian* byte ordering, the initial *instruction pointer* value is 0x0002. So that's where the program starts executing, on the next line.

            Let's decode the instruction at address 0x0002:

            - The **0D** byte is the first one we encounter, which must be the op-code. So that means a 'write' instruction.
            - Next, the **04** byte contains the integer size and the argument types.
                - Bits 0 and 1 are both 0, which means the `integer size` will by 8-bits.
                - Bits 2 and 3 are 1 and 0 respectively, causing `arg1 type` to be 1. That means the literal value for arg1 will be given as `arg1 data`.
                - Bits 4 and 5 are both 0, causing `arg2 type` to be 0. That means that arg2 will have a literal value of 0, and `arg2 data` will not be given.  Note that the 'write' instruction ignores the second argument anyway.
            - The last byte **48** is `arg1 data`. It's a single byte, as `arg1 type` specifies that a literal is given, and the `integer size` says that we're working in 8-bit (1 byte) mode.

            So what this does is write the character with ASCII value 0x48 to the console. Peeking at the <a href="http://www.asciitable.com/">ASCII table</a>, we'll see that 0x48 is the 'H' character. Pfew!

            The rest of the lines a very repetitive. The only thing that changes is the `arg1 data`, reflecting the ASCI codes of the characters we want to print.

            Except the last line, at address 0x0039 which is different:

            - **01** is the op-code, meaning 'set'.
            - **09** for integer size and argument types:
                - `integer size` is 1, meaning 16-bits.
                - `arg1 type` is 2, meaning a single indirection.
                - `arg2 type` is 0, meaning a literal 0.
            - **00 00**: is `arg1 data`. As `arg1 type` says we are using indirection, this data will be a 16-bit memory address, regardless of the size of the data we'll be working on (`integer size`). So the value for the first argument will be located at address 0 in memory.

            So this instructions sets the 16-bit integer at address 0 in memory to the value of 0. Remember that the 16-bit integer at address 0 is the *instruction pointer*. So this means the program execution *jumps* to execute at memory address 0 itself. Which doesn't make sense. But as explained above, this is a special case that *halts* the program. Without this instruction, the virtual CPU would attempt to execute the next (non-existing) instruction after the last 'write'. As memory not initialized by the program would probably just contain zero's, the virtual machine should stop with an error because *op-code* 0 is not defined.

        X marks the spot: |
            Here's another example (also provided as `count.executable`), for a program that writes 42 'x' characters followed by a '\n' (new line) character to the console:

            ```
            0000: 04 00               Instruction pointer, start at 0x0004.
            0002: 2a 00               Data. A 16-bit integer, 0x002a is decimal 42.
            0004: 0d 04 78            Write 'x'.
            0007: 03 19 02 00 01 00   Subtract 1 from the 16-bit value at address 0x0002.
            000d: 0a 09 02 00         Only execute the next instruction if the 16-bit
                                      value at address 0x0002 is larger than 0.
            0011: 01 19 00 00 04 00   Jump back to the instruction at address 0x004 by 
                                      setting the instruction pointer.
            0017: 0d 04 0a            Write '\n', the new line character.
            001a: 01 09 00 00         Jump to address 0x000, causing the program to halt.
            ```

        Hello loopy world: |
            And finally we'll present a program (also provided as `hello_world2.executable`) that does exactly the same as the first Hello world example, but with a loop instead of a lot of repeated 'write' instructions:

            ```
            0000: 11 00                  Instruction pointer, start at 0x0011.
            0002: 48 65 6c 6c 6f 20 77   Data. ASCII: "Hello world!\n"
                  6f 72 6c 64 21 0a
            000f: 02 00                  Data. A 16-bit integer, 0x0002.
            0011: 0d 0c 0f 00            Write the byte at the address the address at 0x000f
                                         refers to (double indirection) to the console.
            0015: 02 19 0f 00 01 00      Add 1 to the 16-bit integer at 0x000f.
            001b: 0b 19 0f 00 0f 00      Only execute the next instruction if the 16-bit integer
                                         at 0x000f is smaller than 0x000f.
            0021: 01 19 00 00 11 00      Jump back to the instruction at address 0x0011.
            0027: 01 09 00 00            Jump to address 0x000, causing the program to halt.
            ```

            Let's assume that the 16-bit integer at address 0x000f is a variable named `current_char`.
            It starts at 2 (the address of the first byte of the "Hello world!" data string). Then we have a loop:

            - Write the byte at the address `current_char` refers to to the console.
            - Increment `current_char` by 1.
            - Loop back if `current_char` is still an address within the "Hello world!\n" string. 0x000f is the first address that no longer belongs to the string.

        Reading and writing the instruction pointer: |
            Reading the instruction pointer (the address at 0x0000) should resolve to the address of the instruction currently being executed. Writing the instruction pointer sets the address of the next instruction to be executed, and should cause the automatic increment to the address of the next instruction not to happen. So for instance, setting the instruction pointer to its current value causes an endless loop on that instruction.

    Assignment:
        -
            ^merge: feature
            0: ""
            1: Only runs `hello_world1`.
            2: Only runs `count` and both `hello_world` examples without problems.
            3: ""
            4: Runs all of five examples like a charm without resorting to hacks specific to these programs.
            text: |
                Based on the scaffolding in `vm.py` implement a virtual SAX16 computer! It should be able to run the provided example executables.

                Hints:
                - Start with the examples shown in the previous section, and add a lot of logging (enabled by the `-d` debug flag provided by the scaffold?) to make sure everything does what you think it does.
                - The expected output (what is printed by the program) is provided for each of the `.executable`s in its corresponding `.output` file.
                - A debug trace of a program's execution is provided in its `.debug` file. You can compare this with your own debug output to see where they start to differ, in case a program doesn't work as expected on your virtual machine.
