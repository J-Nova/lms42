sp: int16 stack

a: int32 0
b: int32 0
c: int32 0
d: int32 0
e: int32 0

macro call FUNCTION {
	set16 **sp *ip # Copy the address of this instruction into **sp
	add16 **sp 24 # skip the 24 bytes for all instruction in this macro
	add16 *sp 2 # Increment the stack pointer
	set16 *ip FUNCTION
}

macro return {
	sub16 *sp 2
	set16 *ip **sp
}

question: "Which prime should I calculate? "
int8 0 # zero-terminate the string

main:
	# Which prime to return
	set16 *a question
	call printString
	call readInt32
	set32 *b *a

	set32 *c 3

	# As we're only checking uneven number normally (as an optimization), we need
	# to cheat a little when the first prime is requested:
	if<32 *b 2 set32 *c 2
	
outer:
	set32 *d 1
inner:
	# if (*d += 2) >= *c goto bingo
	add32 *d 2
	if>32 *d *c set16 *ip bingo
	if=32 *d *c set16 *ip bingo
	
	# if (*c % *d) == 0 goto nextNumber
	set32 *e *c
	div32 *e *d
	mul32 *e *d
	if=32 *e *c set16 *ip nextNumber

	set16 *ip inner

bingo:
	# We've got one!
	if<32 *b 3 set16 *ip printAnswer
	sub32 *b 1

nextNumber:
	add32 *c 2
	if<32 *c 1000000 set16 *ip outer
	set16 *ip 0

printAnswer:
	set32 *a *c
	call printInt32
	write8 '\n'
	set16 *ip 0

# printString
# in:
#   a: address of the zero-terminated string
# out:
#   a: points at the terminating zero
printString:
	if=8 **a 0 set16 *ip printStringExit
	write8 **a
	add16 *a 1
	set16 *ip printString
printStringExit:
	return


	


# printInt32
# in:
#   a: the number to print in decimal
# out:
#   a: will be 0
# destroys: e, any stack with sp>=0
printInt32:
	# *b will point at the next stack position to write a character to
	set16 *b *sp

printInt32CalcLoop:
	# **b = a % 10
	set32 **b *a
	div32 *a 10
	mul32 *a 10
	sub32 **b *a
    add8 **b '0'

	# *b++
	add16 *b 1

	# *a /= 10
	div32 *a 10

	# loop while *a > 0
	if>32 *a 0 set16 *ip printInt32CalcLoop
	
printInt32ShowLoop:
	# write **--b + '0'
	sub16 *b 1
	write8 **b

	# loop while *b > *sp
	if>16 *b *sp set16 *ip printInt32ShowLoop
	
	return


# readInt32
# out:
#   a: the 32 bit number that was read
# destroys: e
readInt32:
	set32 *a 0
readInt32Next:
	# Read a character into e
	read8 *e

	# Did we read a new line character? Stop reading! 
	if=8 *e 10 set16 *ip readInt32Ready

	# Convert to number 0-9, and ignore any other character
	sub8 *e '0'
	if>8 *e 9 set16 *ip readInt32Next

	# a = *a * 10 + *e
	mul32 *a 10
	add32 *a *e
	set16 *ip readInt32Next
readInt32Ready:
	return

stack:
