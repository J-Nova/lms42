/* use rand::Rng; */


/*
    Your code goes here!
 */



fn main(){
    // You can use this to experiment, or just leave it empty.
}

/*
#[test]
fn u32_is_hashable() {
    assert_eq!(17.get_hash(), 17.get_hash());
    assert_ne!(17.get_hash(), 18.get_hash());
}
*/

/*
#[test]
fn hash_works_with_u32() {
    let mut hash = Hash::new();

    hash.add(1);
    hash.add(1234);
    hash.add(12345678);
    assert!(hash.contains(1));
    assert!(hash.contains(1234));
    assert!(hash.contains(12345678));
    assert!(!hash.contains(2));
    assert!(!hash.contains(1235));
    assert!(!hash.contains(12345679));
}
*/

/*
#[test]
fn hash_scales_with_u32() {
    let mut rng = rand::thread_rng();
    let mut hash = Hash::new();
    let mut reference = std::collections::HashSet::new();

    for _ in 0..500_000 {
        let n = rng.gen_range(0..2_000_000) as u32;
        hash.add(n);
        reference.insert(n);
    }

    for num in 0..2_000_000 {
        assert_eq!(hash.contains(num), reference.contains(&num));
    }
}
*/

/*
#[test]
fn str_ref_is_hashable() {
    assert_eq!("abcdefghijklmnopqrstuvwxyz".get_hash(), "abcdefghijklmnopqrstuvwxyz".get_hash());
    assert_ne!("abcdefghijklmnopqrstuvwxyz".get_hash(), "abcdefghijklmnopqrstuvwxy".get_hash());
    assert_ne!("abcdefghijklmnopqrstuvwxyz".get_hash(), "abcdefghijklmnopqrstuvwxyZ".get_hash());
}
*/

/*
#[test]
fn hash_works_with_str_ref() {
    let mut hash = Hash::new();

    hash.add("test");
    hash.add("abcd");

    assert!(hash.contains("test"));
    assert!(hash.contains("abcd"));
    assert!(!hash.contains("tset"));
    assert!(!hash.contains("abc"));
    assert!(!hash.contains("abce"));
    assert!(!hash.contains("abcde"));
}
*/

/*
/// This test does not compile. Fix it by moving a single line. 
/// Explain in comments why this change is required.
#[test]
fn hash_works_with_lifetimes() {
    let mut hash : Hash<&str> = Hash::new();

    let mut x = String::from("abc");
    x.push_str("123");

    hash.add(&x);

    x.push_str("456");

    assert!(hash.contains("abc123"));

    assert_eq!(x, "abc123456");
}
*/