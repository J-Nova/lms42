package com.example.myapplication.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.myapplication.AppDatabase;
import com.example.myapplication.HouseDao;
import com.example.myapplication.R;
import com.example.myapplication.models.House;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    ListView listView;
    EditText streetNameView;
    EditText houseNumberView;
    EditText askingPriceView;
    EditText sizeView;
    Button addButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);
        streetNameView = findViewById(R.id.streetNameView);
        houseNumberView = findViewById(R.id.houseNumberView);
        askingPriceView = findViewById(R.id.askingPriceView);
        sizeView = findViewById(R.id.sizeView);
        addButton = findViewById(R.id.addButton);

        AppDatabase db = AppDatabase.getInstance(this);

        HouseDao dao = db.houseDao();

        dao.getAllHousesWithBids().observe(this, new Observer<List<HouseDao.HouseWithBids>>() {
            @Override
            public void onChanged(List<HouseDao.HouseWithBids> houses) {
                listView.setAdapter(new ArrayAdapter<HouseDao.HouseWithBids>(MainActivity.this, android.R.layout.simple_list_item_1, houses));
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                House house = (House)listView.getAdapter().getItem(position);
                Intent intent = new Intent(MainActivity.this, BidActivity.class);
                intent.putExtra("id", house.id);
                startActivity(intent);
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                House house = new House();
                house.streetName = streetNameView.getText().toString();
                house.houseNumber = Integer.parseInt(houseNumberView.getText().toString());
                house.askingPrice = Integer.parseInt(askingPriceView.getText().toString());
                house.size = Integer.parseInt(sizeView.getText().toString());
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        dao.addHouse(house);
                    }
                }).start();
            }
        });
    }
}