name: A more complex design
description: Create interview questions, a domain model and a UX design for a more complex case study.
goals:
    interview: 1
    wireframing: 1
    datamodeling: 1
days: 2
assignment:

    - Assignment: |
        In this assignment you are asked to create the domain model and wire frames for the application request (board game extension). Watch the video below to see what the customer wants you to build.

        - [Application Request: Board game extension](https://video.saxion.nl/media/Board%20game%20extension/1_wwqkcxm8)

    - Take notes:
      -
        must: true
        text: |
          Write your notes in `notes.txt`.
       
    - Interview questions:
      - 
        text: |
          Your tasks:
          - Create a list of at least 5 questions you would like to ask the customer to clarify some of the features (in template folder you will find `questions.txt`). Be specific in your questions. Make sure your questions are clear and relevant.
          - Provide plausible answers to these question. Unfortunately the customer will not be able to answer your questions. Provide answers to your questions to the best of your ability. It's fine if your answers are not entirely correct.


          *Tips*:
          - Formulate open question instead of closed question
          - Don't formulate question for which the answer can be found in the video
          - Be sure that your questions are not too similar
        map:
          interview: 1
        0: No questions
        2: Half of the questions are relevant or answers are not well though out.
        4: Relevant question including well thought out answers.

    - Domain model:
      -
        link: https://www.youtube.com/watch?v=jtWj-rreoxs
        title: Map out a Domain Model
        info: Introduction into how map entities from use cases to a domain model.
      -
        text: |
          Create the domain model using PlantUML (see the file `domain_model.md` in the template folder).


        0: Domain model does not give a clear impression of the application to be build
        4: Domain contains all entities with proper relationship (including correct cardinality).
        map:
          datamodeling: 1


    - Wireframes:
      - 
        link: https://www.justinmind.com/blog/low-fidelity-vs-high-fidelity-wireframing-is-paper-dead/
        title: "Low fidelity vs high fidelity wireframes: what's the difference?"
        info: A blog explaining the difference between low - and high fidelity wireframes and what the benefits of each is.
      -
        text: |
          Your tasks:
          - Create a **low fidelity** wireframe for every screen in your application.
          - For every screen you should explain (*in a note next to the screen*) the following:
            1. Which elements are interactive (though this should be obvious in your wireframe).
            2. For each interactive element the effect it has in the application. For example a a "Save" button in your application will save information in an applications database.You should describe these actions. Although this "Save" example is straightforward some actions a not (for example a push notification that is sent to a different user when a message is created in the application).
        
        0: Wireframes do not give a clear impression of the application to be build
        4: Clear wireframes including intuitive navigation between pages.
        map:
          wireframing: 1
        
        