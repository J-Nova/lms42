# This allows you to use circular dependencies in type hints.
from __future__ import annotations

import blessed, time
from abc import ABC, abstractmethod

FPS = 30 # Frames per second.

# What follows is an example to help you get started with the main loop.
# It is *NOT* object-oriented yet.

term = blessed.Terminal()

with term.fullscreen(), term.cbreak(), term.hidden_cursor():
    pressed_keys = []
    while True:
        # Clear the screen.
        print(term.clear, end="")

        # Draw whatever you want to draw.
        # It usually a good idea to use end="" to prevent printing a new line.
        print(term.cyan("Hi mom!"), end="")
        print(term.move(10,10) + term.yellow("Pressed keys: ") + " ".join(pressed_keys), end="")

        # After everything has been printed, make sure to flush the print
        # output buffer. This may not happen automatically if your last
        # print didn't end in a new line character.
        print("", end="", flush=True)

        # Wait for 1/FPS seconds, and append all keys that were
        # pressed within that time as strings in pressed_keys.
        pressed_keys.clear()
        until_time = time.time() + 1/FPS
        while True:
            remaining_time = until_time - time.time()
            if remaining_time <= 0:
                break
            key = term.inkey(remaining_time)
            if key:
                pressed_keys.append(key.name or str(key))
