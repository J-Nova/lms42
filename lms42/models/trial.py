from ..app import db, app, get_base_url
import datetime
from ..utils import generate_password, format_date
from ..email import send as send_email
import flask


class TrialDay(db.Model):
    date = db.Column(db.Date, primary_key=True)

    slots = db.Column(db.Integer, nullable=False, default=2)


class TrialStudent(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    secret = db.Column(db.String, nullable=False, default=generate_password)

    first_name = db.Column(db.String, nullable=False)
    last_name = db.Column(db.String, nullable=False)
    email = db.Column(db.String, nullable=False, unique=True)

    invitation_time = db.Column(db.DateTime, default=datetime.datetime.utcnow, nullable=False)

    trial_day_date = db.Column(db.Date, db.ForeignKey('trial_day.date'), nullable=True)
    trial_day = db.relationship("TrialDay", backref="students")

    student_id = db.Column(db.Integer)
    submit_date = db.Column(db.Date)
    modify_date = db.Column(db.Date)
    city = db.Column(db.String)
    enrollment_date = db.Column(db.Date)
    enrollment_state = db.Column(db.String)
    admission_state = db.Column(db.String)
    nationality = db.Column(db.String)
    correspondence_city = db.Column(db.String)
    correspondence_country = db.Column(db.String)
    language = db.Column(db.String)
    prior_education = db.Column(db.String)

    comment = db.Column(db.String)
    manual_probability = db.Column(db.Integer)


    @property
    def url(self):
        return f"{get_base_url()}/trial/{self.id}/{self.secret}"


    @property
    def probability(self):
        if self.manual_probability != None:
            return self.manual_probability
        if not self.enrollment_state: # Not enrolled yet (but registered for trial on open day)
            if self.trial_day_date:
                return 50
            else:
                return 70
        if self.language == 'Nederlands' or self.correspondence_country == 'Nederland' or self.nationality == 'Nederland':
            if self.trial_day_date:
                return 80
            else:
                return 60
        else:
            if self.trial_day_date:
                return 40
            else:
                return 2


    def send_invite_email(self):
        send_email(self.email, "Saxion invitation", f"""Hi {self.first_name},

You are warmly invited to participate in a trial day for the Associate degree Software Development at Saxion. Please refer to this link for more information and to schedule your visit:

{self.url}

If you are unable to attend a trial day due to other commitments or because you are located abroad, please don't hesitate to contact me to arrange an online intake or to discuss alternative dates.

To ensure that you've made the right choice while you still have other options, we recommend that you make an appointment as soon as possible. We're looking forward to your visit!

Best regards,

ir. Frank van Viegen
Ad Software Development
Saxion University of Applied Sciences

If you have any questions, please feel free to contact me at f.c.vanviegen@saxion.nl.
""")



    def send_scheduled_email(self):
        send_email(self.email, "Trial day scheduled", f"""Hi {self.first_name},
                
You scheduled your trial day for the Saxion Ad Software Development on {format_date(self.trial_day_date)}.

Instructions for your visit:
- We're located at Van Galenstraat 19, Enschede. Take the elevators to the 5th floor, and turn left into the long corridor. Our office is at the end, in room G5.27.
- Please try to arrive between 8:45 and 9:00 in the morning. The day ends between 16:00 and 17:00, although you're of course free to leave at any time.
- Bring your laptop and headphones.
- You may want to bring lunch, or you can buy some in the canteen.
- In case you need to cancel/reschedule, please do so as far in advance as you can, by visiting the scheduling page again: {self.url}

We're looking forward to your visit!

Best regards,

Ad Software Development
Saxion

Any questions? Feel free to contact Frank van Viegen at f.c.vanviegen@saxion.nl.
""")


    def send_cancel_email(self, date):
        send_email(self.email, "Trial day cancelled", f"""Hi {self.first_name},
    
Your Saxion Ad Software Development trial day appointment on {format_date(date)} has been cancelled.

Best regards,

Ad Software Development
Saxion

Any questions? Feel free to contact Frank van Viegen at f.c.vanviegen@saxion.nl.
""")
