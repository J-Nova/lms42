from ..app import db
from ..routes import discord
import datetime


class Grading(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    attempt_id = db.Column(db.Integer, db.ForeignKey('attempt.id'), nullable=False, index=True)
    attempt = db.relationship("Attempt", backref=db.backref("gradings", lazy="dynamic"))

    time = db.Column(db.DateTime, default=datetime.datetime.utcnow, nullable=False)

    grader_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False, index=True)
    grader = db.relationship("User", foreign_keys="Grading.grader_id")

    grade = db.Column(db.Integer, nullable=False)
    grade_motivation = db.Column(db.String)

    passed = db.Column(db.Boolean, nullable=False)

    objective_scores = db.Column(db.ARRAY(db.Float), nullable=False)
    objective_motivations = db.Column(db.ARRAY(db.String))

    needs_consent = db.Column(db.Boolean, nullable=False, default=False)
    consent_user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    consenter = db.relationship("User", foreign_keys="Grading.consent_user_id")
    consent_time = db.Column(db.DateTime)

    def send_grade_dm(self):
        if self.attempt.student.discord_id:
            if self.attempt.status == "passed":
                content = f"You passed {self.attempt.node_name} with a {round(self.grade)}!"
            else:
                content = f"You failed {self.attempt.node_name} with a {round(self.grade)}."
            discord.send_dm(discord_id=self.student.discord_id, title=f"An exam has been reviewed by {self.grader.short_name}.", content=content)


    def send_consent_dm(self):
        # Send dms to all teachers.
        teachers = User.query \
                        .filter_by(is_active=True, is_hidden=False, level=50) \
                        .filter(User.id != self.grader_id) \
                        .all()
        content = f"Exam {self.attempt.node_name} by {self.attempt.student.short_name} needs grading consent."
        for teacher in teachers:
            if teacher.discord_id:
                discord.send_dm(discord_id=teacher.discord_id, title="Exam grading", content=content)

from .user import User