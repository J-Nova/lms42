from ..app import db, app
from ..working_days import get_current_octant, get_octant_dates
import sqlalchemy as sa
import flask

@app.route('/tv', methods=['GET'])
@app.route('/tv/<device>', methods=['GET'])
def tv(device=None):
    fragment = flask.request.args.get('fragment')
    if fragment in ('octant-stats',):
        template = fragment
    else:
        template = 'tv'
    return flask.render_template(template+'.html', header=False, device=device, octant_stats=get_octant_stats(device))


def get_octant_stats(device):

    year, octant = get_current_octant()
    start_date, end_date = get_octant_dates(year, octant)

    classes = [f"ESD1V.{letter}" for letter in ([device] if device in ['a', 'b'] else ['a','b'])]

    with db.engine.connect() as dbc:
        sql = """
        select coalesce(sum(a1.avg_days),0)::integer as progress, coalesce(sum(a1.credits),0) as ects
        from attempt a1
        left join attempt a2 on a1.student_id=a2.student_id and a1.node_id=a2.node_id and a1.status='passed' and a2.submit_time < a1.submit_time
        join "user" u on u.id = a1.student_id and u.class_name = any(:classes)
        where a1.status='passed' and a1.submit_time >= :start_date and a2.node_id is null
        """
        octant_stats = dict(dbc.execute(sa.sql.text(sql), start_date=start_date, classes=classes).fetchone())

        sql = """
        select (coalesce(sum(extract(epoch from end_time-start_time)),0) / 3600)::integer hours
        from time_log t
        join "user" u on u.id=t.user_id
        where u.class_name = any(:classes) and t.date >= :start_date
        """
        octant_stats.update(dbc.execute(sa.sql.text(sql), start_date=start_date, classes=classes).fetchone())

        sql = """
        select
            count(*) filter(where a1.status='needs_grading' or a1.status='needs_consent') as gradable
        from attempt a1
        join "user" u on u.id = a1.student_id and u.class_name = any(:classes) and u.level=10 and u.is_active=true and u.is_hidden=false
        join time_log t on t.user_id=u.id and t.date=current_date
        where (a1.status='needs_grading' or a1.status='needs_consent')
        """
        octant_stats.update(dbc.execute(sa.sql.text(sql), start_date=start_date, classes=classes).fetchone())

    return octant_stats


