from ..app import app, get_base_url
import flask
import os
import re
import requests
import hashlib
import base64
import zlib


CACHE_DIR = f"{os.getcwd()}/{app.config['DATA_DIR']}/plantuml_cache"
os.makedirs(CACHE_DIR, exist_ok=True)


# The functions were borrowed from plantweb.

def _compress(content_bytes):
    zlibbed_str = zlib.compress(content_bytes)
    return zlibbed_str[2:-4]

def _encode(data):
    """
    Encode given data into PlantUML server encoding.

    This algorithm is similar to the base64 but custom for the plantuml server.

    :param bytes data: Data to encode.
    :return: The encoded data as printable ASCII.
    :rtype: str
    """
    res = ''
    for i in range(0, len(data), 3):
        if i + 2 == len(data):
            res += _encode3bytes(
                data[i],
                data[i + 1],
                0
            )
        elif i + 1 == len(data):
            res += _encode3bytes(
                data[i],
                0, 0
            )
        else:
            res += _encode3bytes(
                data[i],
                data[i + 1],
                data[i + 2]
            )
    return res

def _encode3bytes(b1, b2, b3):
    c1 = b1 >> 2
    c2 = ((b1 & 0x3) << 4) | (b2 >> 4)
    c3 = ((b2 & 0xF) << 2) | (b3 >> 6)
    c4 = b3 & 0x3F
    res = ''
    res += _encode6bit(c1 & 0x3F)
    res += _encode6bit(c2 & 0x3F)
    res += _encode6bit(c3 & 0x3F)
    res += _encode6bit(c4 & 0x3F)
    return res

def _encode6bit(b):
    if b < 10:
        return chr(48 + b)
    b -= 10
    if b < 26:
        return chr(65 + b)
    b -= 26
    if b < 26:
        return chr(97 + b)
    b -= 26
    if b == 0:
        return '-'
    if b == 1:
        return '_'


@app.route('/plantuml/<path:code_base64>', methods=['GET'])
def get_plantuml(code_base64):

    theme = 'dark' if flask.request.args.get('theme') == 'dark' else 'light'
    style_file = f"style-{theme}.plantuml"
    code_bytes = base64.b64decode(code_base64)

    sha = hashlib.sha1(code_bytes).hexdigest()
    time = int(os.path.getmtime(style_file))
    cache_file = f"{CACHE_DIR}/{sha}-{theme}-{time}.png"

    if not os.path.isfile(cache_file):
        if not flask.request.headers.get('Referer','').startswith(get_base_url()):
            return "Expected referrer: "+get_base_url(), 403

        with open(style_file, "rb") as file:
            stylesheet = file.read()
        code_bytes = stylesheet + "\n".encode('ascii') + code_bytes

        encoded = _encode(_compress(code_bytes))

        url = "http://www.plantuml.com/plantuml/png/" + encoded
        response = requests.get(url)
        with open(cache_file, 'wb') as file:
            file.write(response.content)

    return flask.send_file(cache_file, cache_timeout=86400)
