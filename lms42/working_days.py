import datetime
from collections import defaultdict
from .utils import local_to_utc

START_TIME = {"hour": 8, "minute": 30, "second": 0, "microsecond": 0}
END_TIME = {"hour": 17, "minute": 0, "second": 0, "microsecond": 0}
DEADLINE_TIME = {"hour": 17, "minute": 30, "second": 0, "microsecond": 0}
LUNCH_TIME = {"hour": 12, "minute": 15, "second": 0, "microsecond": 0}

VACATIONS = [
    # 2020/2021
    ('2020-10-12', '2020-10-16'),
    ('2020-12-21', '2021-01-01'),
    ('2021-02-22', '2021-02-26'),
    '2021-04-02',
    '2021-04-05',
    '2021-04-27',
    ('2021-05-03', '2021-05-07'),
    '2021-05-13',
    '2021-05-14',
    '2021-05-24',
    ('2021-07-19', '2021-08-27'),

    # 2021/2022
    ('2021-08-30', '2021-08-31'), # sd intro
    ('2021-10-18', '2021-10-22'),
    ('2021-12-27', '2022-01-07'),
    ('2022-02-21', '2022-02-25'),
    '2022-04-15',
    '2022-04-18',
    '2022-04-27',
    ('2022-05-02', '2022-05-06'),
    '2022-05-26',
    '2022-05-27',
    '2022-06-06',
    ('2022-07-18', '2022-09-02'),

    # 2022/2023
    '2022-09-05', # sd intro
    ('2022-10-17', '2022-10-21'), # herfst
    ('2022-12-26', '2023-01-06'), # kerst
    ('2023-02-27', '2023-03-03'), # voorjaar
    '2023-04-07', # goede vrijdag
    '2023-04-10', # 2e paasdag
    '2023-04-27', # koningsdag
    ('2023-05-01', '2023-05-05'), # mei
    ('2023-05-18', '2023-05-19'), # hemelvaart
    '2023-05-29', # pinkster
    ('2023-07-20', '2023-07-21'), # last two days of 4.11 free
    ('2023-07-24', '2023-09-01'), # zomer+hoi

    # 2023/2024
    '2023-09-04', # sd intro
    ('2023-10-23', '2023-10-27'), # herfst
    ('2023-12-25', '2024-01-05'), # kerst
    ('2024-02-19', '2024-02-23'), # voorjaar
    '2024-03-29', # goede vrijdag
    '2024-04-01', # 2e paasdag
    ('2024-04-29', '2024-05-03'), # mei
    ('2024-05-09','2024-05-10'), # hemelvaart
    '2024-05-20', # pinkster
    '2024-07-19', # last friday of 4.11 free
    ('2024-07-22', '2024-08-30'), # zomer+hoi
]



def calculate_vacation_days():
    days = set()
    for item in VACATIONS:
        if isinstance(item, str):
            days.add(datetime.date.fromisoformat(item))
        else:
            date = datetime.date.fromisoformat(item[0])
            end_date = datetime.date.fromisoformat(item[1])
            while date <= end_date:
                days.add(date)
                date += datetime.timedelta(days=1)
    return days

# Precalculate a set containing all vacation days
VACATION_DAYS = calculate_vacation_days()

# The last academic year for which we know the dates
MAX_ACADEMIC_YEAR = max(VACATION_DAYS).year - 1


def is_working_week(date: datetime.date):
    monday = date + datetime.timedelta(days = (7 - date.weekday()) % 7)
    for offset in range(0,5):
        if (date + datetime.timedelta(days=offset)) not in VACATION_DAYS:
            return True
    return False


def is_working_day(date: datetime.date):
    return date.weekday() not in [5,6] and date not in VACATION_DAYS


def offset(date: datetime.date, days: int):
    while not is_working_day(date):
        date += datetime.timedelta(days=1)

    offset = 1
    if days < 0:
        offset = -1
        days = -days

    for _ in range(days):
        date += datetime.timedelta(days=offset)
        while not is_working_day(date):
            date += datetime.timedelta(days=offset)
    return date


def calculate_per_month(date: datetime.date, end_date: datetime.date = datetime.date.today()):
    result = defaultdict(int)
    while date < end_date:
        if is_working_day(date):
            result[f"{date.year}-{date.month:02}"] += 1
        date += datetime.timedelta(days=1)
    return dict(result)


def get_working_hours_delta(start: datetime.datetime, end: datetime.datetime, ignore_dates: set = set()):
    """Get the number of (fractional) working hours between `start` and `end`,
    both in timezone naive UTC.""" 
    seconds = 0
    while start.date() <= end.date():
        day_end = end if start.date()==end.date() else local_to_utc(start.replace(**END_TIME))
        day_rest = (day_end - start).seconds if day_end > start else 0
        if start < local_to_utc(start.replace(**LUNCH_TIME)) < day_end:
            day_rest -= 1800 # subtract half an hour lunch time
        seconds += max(0, day_rest)

        next_date = offset(start.date(), 1)
        while next_date in ignore_dates and next_date < end.date():
            next_date = offset(next_date, 1)
    
        start = local_to_utc(datetime.datetime(next_date.year, next_date.month, next_date.day, **START_TIME))

    return seconds / 3600

def calculate_week_starts():
    week_starts = {}
    monday = datetime.date(2020, 8, 31) # The start of the 2020/2021 academic year.
    last_vacation_day = sorted(VACATION_DAYS)[-1] + datetime.timedelta(days=7)
    week = 1
    year = 2020
    while monday < last_vacation_day:
        if is_working_week(monday):
            week_starts.setdefault(year, {})
            week_starts[year][week] = monday
            week += 1

        monday += datetime.timedelta(days=7)
        if monday.month >= 8 and year < monday.year:
            year = monday.year
            week = 1
    return week_starts

def print_week_starts():
    for year, weeks in calculate_week_starts().items():
        print(year)
        for week, start in weeks.items():
            print(f"  {week}: {start}")


# Precalculate the Monday dates of the education week numbers per year
WEEK_STARTS = calculate_week_starts() # {year: {week: monday_date}}


def get_quarter_dates(year, quarter, q4="whole_year"):
    """Returns a tuple with the first day of the quarter and day after the quarter specified 
    by `year` and `quarter`. When `year` is 2021 and `quarter` is 4, it would return the
    dates of the last quarter of the 2021/2022 academic year.
    
    :param year: The academic year. (So 2021 for the 2021/2022 year.)
    :param quarter: The quarter (1-4).
    :param q4: How to calculate the end date for the 4th period.
        "whole_year": there is no gap between q4 and q1, with the transition being 2 weeks before
            the strict start of q1. (default)
        "strict": until the first date of 'week 11'
        "extra_week": until the first date of 'week 12'
    """
    
    start = WEEK_STARTS[year][(quarter-1) * 10 + 1]
    end = WEEK_STARTS[year][(quarter-1) * 10 + 11]
    if quarter == 4:
        if q4 == "whole_year":
            end = WEEK_STARTS[year+1][1] + datetime.timedelta(days=-14)
        elif q4 == "extra_week":
            end += datetime.timedelta(days=7)
        elif q4 != "strict":
            raise ValueError("Invalid q4 parameter")
    elif quarter == 1 and q4 == "whole_year":
        start += datetime.timedelta(days=-14)
    return start, end


def get_octant_dates(year, octant, q4="whole_year"):
    """Returns a tuple with the first day of the octant and day after the octant specified 
    by `year` and `octant`. When `year` is 2021 and `octant` is 8, it would return the
    dates of the last quarter of the 2021/2022 academic year.
    
    :param year: The academic year. (So 2021 for the 2021/2022 year.)
    :param octant: The octant (1-8).
    :param q4: How to calculate the end date for the 4th period.
        "whole_year": there is no gap between q4 and q1, with the transition being 2 weeks before
            the strict start of q1. (default)
        "strict": until the first date of 'week 6'
        "extra_week": until the first date of 'week 7'
    """
    
    start = WEEK_STARTS[year][(octant-1) * 5 + 1]
    end = WEEK_STARTS[year][(octant-1) * 5 + 6]
    if octant == 8:
        if q4 == "whole_year":
            end = WEEK_STARTS[year+1][1] + datetime.timedelta(days=-14)
        elif q4 == "extra_week":
            end += datetime.timedelta(days=7)
        elif q4 != "strict":
            raise ValueError("Invalid q4 parameter")
    elif octant == 1 and q4 == "whole_year":
        start += datetime.timedelta(days=-14)
    return start, end


def get_current_quarter(day=None):
    if day==None:
        day = datetime.date.today()
    for year in (day.year-1, day.year):
        for quarter in [1,2,3,4]:
            start,end  = get_quarter_dates(year, quarter)
            if start <= day < end:
                return year, quarter
    raise Exception("Couldn't determine quarter")


def get_current_octant(day=None):
    if day==None:
        day = datetime.date.today()
    for year in (day.year-1, day.year):
        for octant in range(1,9):
            start,end  = get_octant_dates(year, octant)
            if start <= day < end:
                return year, octant
    raise Exception("Couldn't determine octant")


def date_range(start, end):
    day = offset(start, 0)
    while day < end:
        yield day
        day = offset(day, +1)


if __name__ == "__main__":
    # poetry run python -m lms42.working_days  
    print_week_starts()
    for year in range(2020, MAX_ACADEMIC_YEAR+1):
        start, _ = get_quarter_dates(year, 1, "extra_week")
        _, end = get_quarter_dates(year, 4, "extra_week")
        day_count = 0
        for day in date_range(start, end):
            if is_working_day(day):
                day_count += 1
        print(f"{year}: {start} - {end} --> {day_count} working days")
