import logging.config
import os
import sys
import alembic
import sqlalchemy

sys.path.append(os.getcwd())

config = alembic.context.config

logging.basicConfig(
    level=logging.INFO, 
    format='%(message)s',
    handlers=[logging.StreamHandler(sys.stdout)]
)

from lms42.app import db, app

config.set_main_option("sqlalchemy.url", app.config["SQLALCHEMY_DATABASE_URI"])

target_metadata = db.metadata


def run_migrations_offline():
    """Run migrations in 'offline' mode.
    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.
    Calls to context.execute() here emit the given string to the
    script output.
    """

    url = config.get_main_option("sqlalchemy.url")
    alembic.context.configure(url=url)

    with alembic.context.begin_transaction():
        alembic.context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.
    In this scenario we need to create an Engine
    and associate a connection with the context.
    """

    engine = sqlalchemy.engine_from_config(
        config.get_section(config.config_ini_section),
        prefix='sqlalchemy.',
        poolclass=sqlalchemy.pool.NullPool
    )
    connection = engine.connect()
    alembic.context.configure(connection=connection, target_metadata=target_metadata)

    try:
        with alembic.context.begin_transaction():
            alembic.context.run_migrations()
    finally:
        connection.close()

if alembic.context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
